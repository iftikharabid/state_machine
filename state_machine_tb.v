`timescale 100ns / 1ps
module state_machine_tb();
reg clock;
reg reset;
reg input1;
reg input2;

wire [1:0] led;
wire [2:0] counter;
wire [1:0] current_state;
wire [1:0] next_state;

//Initialise variables
initial 
begin
	$dumpfile ("state_machine_tb.vcd");
	$dumpvars (0, state_machine_tb, dut);
//$display("time\t clock reset enable counter");
//$monitor("%g\t %b %b %b %b", $time, clock, reset, enable, read_data);

clock = 1;
reset = 1;
input1 = 0;
input2 = 0;
#2 
reset = 0;
#8 
input1 = 1;
#4 
input1 = 0;
#4 
input2 = 1;
#4 
input2 = 0;
#4 
input1 = 1;
#4 
input2 = 1;
#4 
input1 = 0;
#4 
input2 = 0;
#10 
input1 = 1;
#16
input2 = 1;
#4 
input1 = 0;
input2 = 0;

#10 $finish;

end


//Clock Generator
always 
begin 
#1 clock = ~clock; //Toggle clock each 5 ticks
end

//Connect DUT to Test Bench
state_machine dut (
.clock(clock), 
.reset(reset), 
.input1(input1),
.input2(input2),
.led(led),
.counter(counter),
.current_state(current_state),
.next_state(next_state)
);

endmodule
