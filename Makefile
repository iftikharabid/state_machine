STMCHN := state_machine
vfile  := state_machine.v 
testbench := state_machine_tb.v
lib := mylib_a
TARGETS := clean config compile vsim
RM     := rm -rfv

.PHONY: all
all: $(TARGETS)
	$(info    Performing make ALL)

.PHONY: clean
clean:
	-$(RM) transcript vsim.wlf *.vvp *.vcd *~
	-$(RM) $(lib)

.PHONY: config
config:
	vlib $(lib)
	vmap work $(lib)

.PHONY: compile
compile:
	vlog $(STMCHN).v $(STMCHN)_tb.v
	

.PHONY: vsim
vsim: $(modelsim.ini)
	vsim $(testbench)

.PHONY: gtk
gtk: $(STMCHN)_tb.v $(STMCHN).v
	iverilog -o $(STMCHN)_tb.vvp $^		
	vvp -n $(STMCHN)_tb.vvp +vcp -lxt2
	gtkwave $(STMCHN)_tb.vcd $(STMCHN)_tb.sav

