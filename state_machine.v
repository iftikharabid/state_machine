module state_machine
(
input wire clock, 
input wire reset, 
input wire input1,
input wire input2,
output reg [1:0] led,
output reg [2:0] counter,
output reg [1:0] current_state,
output reg [1:0] next_state
);

localparam STATE0 = 0;
localparam STATE1 = 1;
localparam STATE2 = 2;
localparam STATE3 = 3;

initial
	begin
		counter = 0;
		current_state = STATE0;
		next_state = STATE0;
		led = 0;
	end

//State Transitioning Logic (Combinatorial)
always @(*)
	begin   
		case (current_state)
			STATE0:
				begin
					if (counter == 5)
						begin next_state = STATE1; end
					
				end
			STATE1:
				begin
					if (input1 == 1)
						begin next_state = STATE2; end				
				end
			STATE2:
				begin
					if (input2 == 1)
						begin next_state = STATE1; end
					else if (counter == 5)
						begin next_state = STATE3; end
				end
			STATE3:
				begin
					if (input1 == 0)
						begin next_state = STATE1; end
					else if (input2 == 1)
						begin next_state = STATE0; end
				end
			default:
				begin next_state = STATE1; end


		endcase
	end

//Tasks Execution base on state-machine current state
always @(posedge clock)
	begin  
		if (reset)
			begin
				current_state <= STATE0;
				counter <= 0;
			end

		else 
			begin
				if (counter >= 5)
					begin counter <= 0; end
				else counter <= counter + 1;
				current_state <= next_state; 

				case(current_state)
					STATE0:
						begin led <= 0; end
					STATE1:
						begin led <= 1; end
					STATE2:
						begin led <= 2; end
					STATE3:
						begin led <= 3; end
					default:
						begin led <= 0; end
				endcase

			end
	end

endmodule